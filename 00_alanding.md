---
title: GiS-Lab Marburg
layout: landing
description: 'gisma spatial science ressources<br/> courses blogs & opinons.'
image: assets/images/gisma-1.jpg
nav-menu: true
---

<!-- Main -->
<div id="main">

<!-- One -->
<section id="one">
	<div class="inner">
		<header class="major">
			<h2>About</h2>
		</header>
		<p>
		The Geoinformation Science Lab Marburg (gisma) provides access to different teaching and research contents of the spatial science resources working group at the <a href="https://www.uni-marburg.de/en/fb19"> Department of Geography </a>,<a href="https://www.uni-marburg.de/en"> Philipps-University Marburg </a>.  
		<br><br>
The main goal of this entry page is to make geoinformational concepts and content available in different formats - from blog posts to single turorials to complete courses.
The focus is on reproducible scientific method training Education in the fields of environmental informatics, GIS remote sensing and modeling.
		</p>
	</div>
</section>

<!-- Two -->
<section id="two" class="spotlights">
	<section>
		<a href="88_teaching_bol.html" class="image">
			<img src="{% link assets/images/gisma-13.jpg %}" alt="" data-position="center center" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Teaching backgrounds</h3>
				</header>
				<p>
				In pedagogy, there has been a shift in recent decades from the object of learning to the subject of learning. Learners are no longer seen as passive or active recipients, but as active constructors accompanied by crises. Here, an authentic problem orientation plays a decisive role for a successful learning environment. In the best case, the teaching focuses on questions that have meaning for the learners, that make them curious and/or concerned.
				</p>
				<ul class="actions">
					<li><a href="88_teaching_bol.html" class="button">Learn more</a></li>
				</ul>
			</div>
		</div>
	</section>
	<section>
		<a href="Theses_Evaluation.html" class="image">
			<img src="{% link assets/images/gisma-55.jpg %}" alt="" data-position="top center" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Theses & Evaluation</h3>
				</header>
				<p>
				The most important basis is commitment and joy in the (self-imposed) task. Furthermore, you should be willing to prepare an exposé at the beginning of the work, which will serve as a basis for the supervision and outline for the final project. You can be sure that you will be supervised with the same commitment that you put into your own work. 
				</p>
				<ul class="actions">
					<li><a href="Theses_Evaluation.html" class="button">Learn more</a></li>
				</ul>
			</div>
		</div>
	</section>


	<section>
		<a href="01_course_landing.html" class="image">
			<img src="{% link assets/images/gisma-30.jpg %}" alt="" data-position="25% 25%" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Courses</h3>
				</header>
				<p>
				Courses
				</p>
				<ul class="actions">
					<li><a href="01_course_landing.html" class="button">Learn more</a></li>
				</ul>
			</div>
		</div>
	</section>

</section>

<!-- Three 
<section id="three">
	<div class="inner">
		<header class="major">
			<h2>Massa libero</h2>
		</header>
		<p>Nullam et orci eu lorem consequat tincidunt vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus pharetra. Pellentesque condimentum sem. In efficitur ligula tate urna. Maecenas laoreet massa vel lacinia pellentesque lorem ipsum dolor. Nullam et orci eu lorem consequat tincidunt. Vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus amet pharetra et feugiat tempus.</p>
		<ul class="actions">
			<li><a href="generic.html" class="button next">Get Started</a></li>
		</ul>
	</div>
</section>
-->
</div>
