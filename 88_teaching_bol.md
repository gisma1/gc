---
layout: landing
title: The Bologna Syndrome - Deficits and Opportunities 
description: ''
image: assets/images/gisma-20.jpg
nav-menu: true
---



Student workload, fit, teaching capacity, modularization, cumulative examination system, contact time, consecutive study programs, credit point system, transcripts of records, learning outcomes, creditability procedures, ECTS, employability, competence transfer, - this not only smells like, but also means standardization, bureaucratization and above all economization of teaching and learning at the university.

And yet there are also opportunities and scope if they are actively used. The Bologna Process focuses more strongly on the needs of students. Ultimately, in the economized university education, the balancing act between vocational training and scientific specialized education must be achieved. The much-cited and hardly understood competence-centered modular teaching and learning offers the leeway not to lose sight of the Humboldtian ideal of education and yet to do justice to the necessity of practice-oriented, application-related and science-ethically justified education.

## Development of competencies

Against the background of a competence-oriented education, it is not easy to formulate the teaching and learning objectives in a catchy way. However, the following are perhaps the most important aspects:

* Learning defined subject-specific skills and abilities as a basis for each goal-oriented ability to act.
* Integration of specific didactic methods for knowledge acquisition under the assumption that learning is an active, self-directed, constructive, situational and social process.
* Teaching analytical, solution-oriented ways of thinking and acting in order to develop intuitive and creative problem-solving strategies
* Promotion of autonomous, personal development especially with regard to the further development of personal and 
social-communicative competence

## Problems versus solutions

In pedagogy as well as in didactics, there has been a shift in recent decades from the object of learning to the subject of learning. Learners are no longer seen as passive or active recipients, but as active constructors accompanied by crises. Here, an authentic problem orientation plays a decisive role for a successful learning environment. In the best case, the teaching focuses on questions that have meaning for the learners, that make them curious and/or concerned.

A good implementation in university teaching is based, for example, on the teaching concept of Problem Based Learning.

Learning is student-centered and takes place in small groups
A tutor supervises and supports the learning process
Problems form the starting point and stimulus for the learning process; the learner uses them to acquire
the learner acquires the necessary problem-solving skills
Learners acquire new information through self-directed learning.

Directed knowledge transfer is now only one aspect of university education. Instead, students are guided and supported in their active independent examination of issues.

Teaching in the GI-S domain is developed and implemented on this basis. More on this topic can be found in the following literature.

#  Learning Content

It is the intention of the learning/teaching modules linked here to make available to geographers and other interested parties access to contemporary methods of spatio-temporal description, analysis, and interpretation of the world.  In the past decade, a rapid development has taken place Besides AI MEthodes and Big Data, complex physical models and the integration of complex spatio-temporal analyses have increasingly become current state of the scientific craft. The road ahead is challenging. The course offerings linked on these pages are guided by the following guiding questions:

* How can media and geoinformation methods and techniques be integrated, in a professionally adequate manner, into geographic education and scientific application?

* How do I acquire sovereign action competences in the field of spatiotemporal abstraction and modeling?

The didactic design of the various modules is generally oriented toward constructivist learning theory. In contrast to the schooling of the university system resulting from the Bologna process, the courses offered here require a high degree of self-organized and self-motivated learning.
In general, no distinction is made between the MAster and Bachelor offerings. Both courses are based on a strict orientation towards scientific reproducibility and transparency at the most up-to-date methodological level possible. As far as possible, the learning of methods and modeling skills in geography is project-based. The learning materials made available here serve as a basis for classroom teaching, blended learning approaches as well as self-study. 


## References

Ammoneit, R., Reudenbach, C. & Peter, C. (2021): Selbstorganisation von Trampelpfaden im Raum modellierend erfassen – Eine komplexe Aufgabe für Studierende. In: Fögele, J.,  & N. Raschke (Hrsg.): Die Geographie in der schulischen Praxis stärken. Abstractband zum HGD‐Symposium 2020 in Gießen

Ammoneit, R. Reudenbach, C., Turek, A., Nauß, T. & Peter, C. (2019). Geographische Modellierkompetenz - Modellierung von Raum konzeptualisieren. In: GW-Unterricht. 156, S. 19-29. [online](https://doi.org/10.1553/gw-unterricht156s19).

Schulze, U.; Kanwischer, D. & Reudenbach, C. (2013): Essential competences for GIS learning in higher education: a synthesis of international curricular documents in the GIS&T domain. Journal of Geography in Higher Education 37 (2), 257-275

Schulze, U.; Kanwischer, D.; Reudenbach, C. (2011): Technikzentrierte Softwareschulung und/oder problemorientierte Denkweise? Theoretische Überlegungen und didaktische Analysen zur geographischen GIS-Ausbildung. In: Tagungsband HGD-Symposium Ludwigsburg 2011: Räumliche Orientierung, Karten und Geoinformation im Unterricht.

Schulze, U.; Kanwischer, D.; Reudenbach, C. (2010): Bologna – Gefahr oder Chance? Ein Praxisbericht aus der geographischen Hochschullehre zum kompetenzorientierten Lernen mit Geoinformation. In: Das Hochschulwesen, 6.2010.*
