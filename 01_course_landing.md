---
title: Courses
layout: landing
description: 'gisma spatial science ressources'
image: assets/images/gisma-30.jpg
nav-menu: true
---

<!-- Main -->
<div id="main">

<!-- One -->
<section id="one">
	<div class="inner">
		<header class="major">
			<h2>About</h2>
		</header>
		<p>
		

This courses are brought to you from the Geoinformation Science Lab Marburg (gisma) which provides access to different teaching and research contents of the spatial science resources working group at the <a href="https://www.uni-marburg.de/en/fb19"> Department of Geography </a>,<a href="https://www.uni-marburg.de/en"> Philipps-University Marburg </a>.

 The course content is developed and hosted on
 <i class="fa fa-github"></i> <a href="https://github.com/gisma-courses/"> GitHub</a>
 
The responsibility for the content rests with the instructors. Statements, opinions and/or conclusions are the ones from the instructors and do not necessarily reflect the opinion of the representatives of Marburg University.  

		</p>
	</div>

</section>


<!-- Two -->

<!-- NEW -->
<section id="two" class="spotlights">
			<div class="inner">
			<header class="major">
			<h2>Regular Courses</h2>
		</header>
		</div>
	<section>
<style>
img {
  display: block;
  max-width:450px;
  max-height:450px;
    justify-content: center;
  align-items: center;
  width: auto;
  height: auto;
}
</style>
		<a href="https://gisma-courses.github.io/gi-modules/" class="image">
			<img src="{% link assets/images/gi-modules.jpg %}"  alt="" data-position="25% 25%" />
		</a>
		<div class="content">

			<div class="inner">
				<header class="major">
					<h3>Gi-Modules</h3>
				</header>
				<p>
				GI Modules is a blog-based collection of learning content in the broad field of geoinformatics. The blogs are sorted by categories and tags. Under the categories, related content is grouped together. Within the content pages, you can navigate to related articles via the tags.
The contents are therefore thematically associated but not intended as a closed workflow. The course units are composed of individual modules.
				</p>
				<ul class="actions">
					<li><a href="https://gisma-courses.github.io/gi-modules/" class="button">Start Course</a></li>
				</ul>
			</div>
		</div>
	</section>

<!-- NEW -->

	<section>
		<a href="https://gisma-courses.github.io/geoinfo-basis-qgis/" class="image">
			<img src="{% link assets/images/geoinfo.png %}" alt="" data-position="top center" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Basic geoinformatics course(QGIS)</h3>
				</header>
				<p>

Spatio-temporal data and their manipulation, analysis and representation are basic scientific elements of geography. The analyses form the basis for gaining knowledge in research and the resulting media are the central means of communication. Data and data analysis, as well as the modeling of these data, are therefore an integral part of professional understanding. The basal handling of data, preparation, visual exploration, analysis, and presentation represents an important core competency for a geographer's job description. This course is the basic GIS course for geographers.

				</p>
				<ul class="actions">
					<li><a href="https://gisma-courses.github.io/geoinfo-basis-qgis/" >
					<p class="notice--danger">German only | Start Course</p></a></li>
				</ul>
			</div>
		</div>
	</section>

<!-- NEW -->

	<section>
		<a href="https://gisma-courses.github.io/LV-uav-workflow/" class="image">
			<img src="{% link assets/images/LV-UAV-Guide.jpg %}" alt="" data-position="center center" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Micro Remote Sensing </h3>
				</header>
				<p>
				The course Micro Remote Sensing - provides a complete introduction to the reproducible acquisition preprocessing and examplary analysis of high resolution imaging threat data based on open source software.
This course is designed for students of all levels who have a basic knowledge of the organization of their operating system. Programming knowledge is not required but will be helpful for the advanced module Analysis of high resolution aerial data. The focus is on a practical approach to the independent acquisition of high resolution aerial data. Post-processing of products and an introduction to classification and analysis of the generated datasets complete the course.
				</p>
				<ul class="actions">
					<li><a href="https://gisma-courses.github.io/LV-uav-workflow/" class="button">Start Course</a></li>
				</ul>
			</div>
		</div>
	</section>
	
<!-- NEW -->

	<section>
		<a href="https://gisma-courses.github.io/LV-19-050-007/" class="image">
			<img src="{% link assets/images/LV-19-050-007.jpg %}" alt="" data-position="top center" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Geoinformatics methods (Change Detection)</h3>
				</header>
				<p>

The course Methods of Geoinformatics - Change Detection provides a complete introduction to reproducible change detection of land surfaces/land use (change detection) based on R and open source software.
This course is intended for undergraduate students of geography who have a basic knowledge of the organization of their operating system.				</p>
				<ul class="actions">
					<li><a href="https://gisma-courses.github.io/LV-19-050-007/"> <p class="notice--danger">German only | Start Course</p></a></li>
				</ul>
			</div>
		</div>
	</section>

<!-- NEW -->

	<section>
		<a href="https://gisma-courses.github.io/LV-19-d19-006/" class="image">
			<img src="{% link assets/images/LV-19-d19-006.png %}" alt="" data-position="25% 25%" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>LV-19-d19-006 - Advanced GIS (Micro Climate Modeling)</h3>
				</header>
				<p>
				The advanced Geoinformation Systems (aGIS) course is a full term overview of selected methodological approaches in advanced GIS analysis using reproducible research workflows, based on R and Open Source Software and a full integration of GitHub. This course is intended for the master level in Geography to participants who have a working knowledge of the basic organisation of their operating system as well as basic knowledge of GI Concepts and Scripting. The scientific topic is statistical modeling of forest microclimate based on LiDAR and sentinel datasets and station measurements.
				</p>
				<ul class="actions">
					<li><a href="https://gisma-courses.github.io/LV-19-d19-006/" class="button">Start Course</a></li>
				</ul>
			</div>
		</div>
	</section>

<!-- NEW -->

	<section>
		<a href="https://gisma-courses.github.io/r-spatial-econometry-basics/" class="image">
			<img src="{% link assets/images/econ.png %}" alt="" data-position="25% 25%" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Big Data and Spatial Econometrics </h3>
				</header>
				<p>

				Spatio-temporal data analysis, especially of Big Data, and spatio-temporal prediction on these data are typical requirements of spatial econometrics. The adequate analysis of spatial data and the reliable assessment of spatial correlations are the basis for many economic decision-making processes. For communication purposes, suitable visualizations and geomedia must be created. The basic handling of data, preparation, visual exploration, analysis and presentation are taught in this course as an important core competence for spatial economic and social processes.
				</p>
				<ul class="actions">
					<li><a href="https://gisma-courses.github.io/r-spatial-econometry-basics/"> <p class="notice--danger">German only | Start Course</p></a></li>
				</ul>
			</div>
		</div>
	</section>
	
<!-- NEW -->

	<section>
		<a href="https://gisma-courses.github.io/bsc-systemdynamik/" class="image">
			<img src="{% link assets/images/sysdyn.png %}" alt="" data-position="25% 25%" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>System Dynamics - an introduction
Simulation of dynamic processes </h3>
				</header>
				<p>

				The understanding of spatio-temporal processes, the delineation and modeling of appropriate systems, and the simulation of specific system dynamics are of increasing importance in society and science. Geography, as an interdisciplinary field of science, plays an important role here. Analyses carried out on the basis of models form the basis for gaining knowledge in research. The resulting media and their interpretation are furthermore central means of communication in research but also for decision makers and the formation of social opinion.
				</p>
				<ul class="actions">
					<li><a href="https://gisma-courses.github.io/bsc-systemdynamik/"> <p class="notice--danger">German only | Start Course</p></a></li>
				</ul>
			</div>
		</div>
	</section>
	
<!-- NEW -->

	<section>
		<a href="https://geomoer.github.io/geoAI/" class="image">
			<img src="{% link assets/images/geoai.png %}" alt="" data-position="25% 25%" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>GeoAI - use AI to predict spatial patterns</h3>
				</header>
				<p>

				Understanding environmental change and assessing consequences requires spatial information from landscapes. The crucial question is not whether a landscape contains forest, meadow, field, and river, but how they relate to each other spatially. When collecting spatial information in the field, a tradeoff must be made between level of detail, scale, and temporal repetition. Selected processes can either be studied in detail at a very limited number of observation sites or estimated at a generalized scale for a landscape. The constraints loosen when linking local surveys with area-wide remote sensing observations and predicting the locally collected information in space with artificial intelligence methods.
				</p>
				<ul class="actions">
					<li><a href="https://geomoer.github.io/geoAI/"> <p class="notice--danger">German only | Start Course</p></a></li>
				</ul>
			</div>
		</div>
	</section>
	
<!-- NEW -->

	<section>
		<a href="https://gisma-courses.github.io/moer-mpg-envisys-envi-met/" class="image">
			<img src="{% link assets/images/envimet.png %}" alt="" data-position="25% 25%" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Environmental Systems - Microclimate Modeling using ENVI-met </h3>
				</header>
				<p>

				Forest ecosystems have highly variable microclimates that are also significantly different from the surrounding climate. In the forest, direct solar radiation and wind speed are greatly reduced leading to the attenuation of extreme climates. The magnitude and more so the processes of these differences especially in a highly structured and relief forest is significant. This course will attempt to predict the spatiotemporal high-resolution heterogeneity of microclimates using the modeling software ENVI-met.
				https://gisma-courses.github.io/moer-mpg-envisys-envi-met/
				</p>
				<ul class="actions">
					<li><a href="https://gisma-courses.github.io/moer-mpg-envisys-envi-met/" class="button">Start Course</a></li>
				</ul>
			</div>
		</div>
	</section>
	
<!-- NEW -->

	<section>
		<a href="	https://geomoer.github.io/moer-meko/" class="image">
			<img src="{% link assets/images/meko.png %}" alt="" data-position="25% 25%" />
		</a>
		<div class="content">
			<div class="inner">
				<header class="major">
					<h3>Media Competence Environmental Informatics</h3>
				</header>
				<p>

Media and models are basic scientific elements of geography. They form the basis for gaining knowledge in research and serve as a means of communication. Media and models are thus primarily not a pedagogical element, but an integral part of a professional examination of a topic.
				</p>
				<ul class="actions">
					<li><a href="	https://geomoer.github.io/moer-meko/"> <p class="notice--danger">German only | Start Course</p></a></li>
				</ul>
			</div>
		</div>
	</section>
				<div class="inner">
			<header class="major">
			<h2>Irregular Courses</h2>
		</header>
		<p> Coming Soon</p>
		</div>
</section>


<!-- Three -->
<section id="three">
	<div class="inner">
		<header class="major">
			<h2>Oldies but Goodies</h2>
		</header>
		<p>

<h4>GIS Modules Bachelor 2010/2011</h4>

Since they are still in demand they are linked here: GISMA's first two German-language GIS learning modules based on learning theory. 

At that time, the online learning materials were intended to provide a clearly structured knowledge base that was aligned with the course objectives and that demanded and supported self-directed learning.
The online offering was based on a constructivist understanding of didactics in terms of learning theory.
All courses were structured as blended learning modules, i.e., they included attendance phases and self-directed learning times. However, the courses are also suitable for independent study of the subject matter. All courses are available as SCORM/IMS modules for any learning platform. 

Attention due to the over 10 years old technology, there may be errors in the presentation.
Since they are still in demand they are linked here: GISMA's first two German-language GIS learning modules based on learning theory. 

At that time, the online learning materials were intended to provide a clearly structured knowledge base that was aligned with the course objectives and that demanded and supported self-directed learning.
The online offering was based on a constructivist understanding of didactics in terms of learning theory.
All courses were structured as blended learning modules, i.e., they included attendance phases and self-directed learning times. However, the courses are also suitable for independent study of the subject matter. All courses are available as SCORM/IMS modules for any learning platform. 

Attention due to the over 10 years old technology, there may be errors in the presentation.

		</p>
		<ul class="actions">
			<li><a href="https://gisbsc.gis-ma.org/GISBScL1/de/html/index.html" > <p class="notice--danger">German only | GIS Modul 2010</p></a></li>
				<li><a href="https://minibsc.gis-ma.org/GISBScL1/de/html/index.html">  <p class="notice--danger">German only | GIS Modul 2011</p></a></li>
		</ul>

		
	</div>
</section>

</div>
