---
layout: landing
title: Theses & Assessments
description: ''
image: assets/images/gisma-15.jpg
nav-menu: true
---


In the working group, no matter which degree you are aiming for, you can write a thesis according to your choice and motivation.

You can be sure that you will be supervised with the same commitment that you put into your own work. 

Naturally, mainly topics are offered and selected that require significant portions in the application of modern computer-based methods, i.e. data analysis, environmental informatics GI-S, remote sensing and modeling.

However, the range of content is very broad and can be extended at any time by the expertise of internal or external colleagues. You are explicitly welcome to contribute and discuss your own topic.

## What should you bring with you?

The most important basis is commitment and joy in the (self-imposed) task. Furthermore, you should be willing to prepare an exposé at the beginning of the work, which will serve as a basis for the supervision and outline for the final project. If you take on a given topic, you are expected to approach the question with a certain empathy and to work on the task independently. The conceptual solution proposals for sub-problems and the entire problem should come from you and will be coached and accompanied by the AG.



In addition, you will be systematically networked with other supervisors and fellow students to create a constructive environment for discussion.

Of course, there are also formal requirements that you should fulfill. The essential ones are: Scientific transparency and reproducibility.

## Current suggested topics

* Operational derivation of the near-ground wind field in highly reliefed terrain from weather data sets
* Derivation of spatio-cellular patterns to determine microclimates in forested areas.
* Physical modeling of forest microclimates.
* Automated UAV based analysis of landscape structure elements.


# Assessment Guide

Criteria for the preparation and assessment of **project work** in modules of the regular BS/L3 course of study at the Department of Geography, Philipps-Universität Marburg

Chris Reudenbach, Department of Geography Philipps-Universität Marburg reudenbach@uni-marburg.de

## Framework and terms

This assessment guide is intended for students of the Department of Geography at Philipps-Universität Marburg. It pursues the goal of creating a sufficient degree of comparability and transparency for the execution and assessment of examination papers. The guideline regulates bindingly which requirements are placed on students and thus serves as a basis for self-assessment and critical examination of one's own work. In this way, the assessment criteria applied by the course instructor are presented to the students in a comprehensible manner. Although these criteria are the basis for an assessment, this necessarily remains within a subjective framework.

According to the examination regulations (BSc. PO, 2016, L3 PO, 2017), examination papers are, in terms of their objective and scope, both a central piece of coursework in the course of study, as well as evidence of the overall learning success to be assessed. With regard to content and formal aspects, examination achievements are intended to prove that students can independently work on a problem of the subject area using scientific methods within a set period of time (cf. e.g. §11.3, BSc. PO, 2016). If one follows Deiniger, theses can be described as "*examination performances with scientific claim*" (Deiniger et al. 1996:7).

Since the term "**independent scientific work*"*** is not defined in more detail and therefore not reliable with regard to concrete evaluation criteria, the requirements and evaluation criteria placed on the participants are to be reliably outlined and made comprehensible on the basis of concrete criteria, as far as this seems reasonable and possible. In parts, the criteria catalog presented is based on the considerations of Lorenzen (2002) and Boles (2002) as well as an ongoing discussion[^1] within the department.

## The concept of scientificity

The terms **"scientific work"** or **"using scientific methods"** are not defined in complete agreement in the literature and are consequently examined from the most diverse perspectives (seminars, exercises, etc.) during the course of study. Furthermore, there is readily available relevant literature, for example in Backhaus & Tuor (2008), Baade et al. (2005) or also Bänsch (2003), which derive and discuss all essential aspects in detail and provide corresponding sources for further elaboration. In the following, modified list, the requirements resulting for students are compiled in key words following Lorenzen (2002) and are not discussed in more detail for the reasons mentioned above.

Scientific work can be outlined as follows (after Lorenzen, 2002):

- Integration and confident use of the technical/methodological knowledge acquired in the course of study
- Ability to work on and solve a question/problem independently in a systematic and methodically correct manner.
- Ability of discourse and criticism
- Ability to argue and express oneself transparently, logically and concisely
- Ability of a presentation (e.g. article, paper, poster, lecture) of the results that is correct in form and content.

On this basis, the reception and application of "*good scientific practice"* for project work is assumed. For a more in-depth discussion, especially of the often ignored ethical aspects of scientificity (e.g. plagiarism), please refer to the position paper of the DFG (1998). This position has also found its way into the Hessian Higher Education Act (HSchulG 2010), among other things, and is the legal basis for the evaluation of the scientific working methods of professional scientists in the event of any violations.

## Independent scientific problem solving

A central, if not **the central goal of** university education is to impart the key competencies for **creative**, **critical, independent** and **scientifically correct** processing of more or less original problems and issues of the respective discipline. Consequently, such skills are also expected from students for the preparation of examination papers (project papers, theses, etc.). Lorenzen (2002) characterizes these skills and their application with regard to their evaluation as follows: *"Essential for obtaining a good grade is to work on a manageable problem a) within a certain period of time b) independently, albeit supervised by experts, c) using appropriate scientific methods as well as d) presenting it in a recognized scientific form"* (Lorenzen, 2002:3). In order to further limit possible room for interpretation, Lorenzen specifies this statement in the following text:

*"Independence begins with the selection of one of the topics given by the examiner or own thematic proposals and is shown in the delimitation of the topic, in the formulation of the approach to the investigation, in the choice of material and in the decision for the appropriate solution (method). [...].* 

*A paper is considered scientifically correct if all thematic questions listed in the introduction are answered, all findings and assertions are verifiably substantiated and conclusively proven. Arbitrary omission of partial aspects of a topic or incomplete argumentation in the chain of evidence are considered serious scientific deficiencies. Personal views of the author ("prejudices") must be reflected very critically. [...] This includes the requirement of unconditional honesty on the part of the author in disclosing the sources from which findings, arguments, and suggestions were obtained." (*Lorenzen, 2002:4)

The aspects mentioned are a **central canon of** scientific education and university self-image. If there is any ambiguity, please refer to the literature cited above for further details. 

Even in the examination context of project work, violations of this practice are therefore by no means "trivial offenses", but a clear violation of recognized rules and the ethical self-commitment of scientific working methods. The consequences for students, here related to an examination performance, are wide-ranging and extend from a performance that cannot be adequately assessed to legal action (DFG 1998 ; cf. also Philipps-Universität Marburg, Fachbereich Geographie, 2016).

## Project work as examination performance

Examination papers are to be written in a defined manner in terms of form, content and style. If no separate agreements are made, the binding guidelines for the preparation of papers, written homework, diploma, bachelor and master theses in the Department of Geography (Philipps-Universität Marburg, Department of Geography, 2016) apply. The project paper (PA) is the examination performance of the modules MAS/System Dynamics L-Spam in the BA/L3 degree program Geography. The examination performance consists of a the completion of an independently developed project and the submission of a final scientific report. The aim of the PA is to practice **conceptual** and **technical skills in** the context of an application-oriented, individual **scientific** way of working and communicating.

### Contents

The PA should be based on an independently developed problem in consultation with the course instructor. The students should apply the taught techniques and contents concretely with the help of a geoinformation software in the processing of their problem and, if necessary, develop them further.

### Supervisor/Evaluator

The supervision of the project work is provided by the course management and, if necessary, the tutors. They are available for **specific** follow-up and inquiries. The course instructors evaluate the examination performance. 

### Duration and workload

The planned time required to complete the project work is 4 weeks. The weekly workload amounts to 12 hours (2 SWS classroom teaching, 2 SWS preparation, 3 SWS follow-up). Since it is a group work of 1-3 students, it can be assumed for the entire project with an average performance of approx. 100 person working hours/group.

### Repeat

In the event of failure, the examinations may be repeated twice in accordance with the examination regulations. A repeat performance is considered to be a timely rectification to achieve at least an adequate grade, or alternatively the reworking of a topic. If no written agreements to the contrary apply, in the case of a **resubmission** the grade will be calculated from the arithmetic mean of the initial submission and the resubmission, while in the case of a resubmission an independent assessment will be carried out. 

### Rating



The evaluation scheme, translated into grade points, looks like this: 

- **Satisfactory level (5-9 NP)**
   * *Extension level:* the NetLogo template has been optimized by integrating, moving or deleting components. This optimization makes it possible to answer your own question in the first place. 
   * *Question and hypothesis*: A section of space was precisely defined as the object of investigation, and the spatial elements to be studied were named. Expected patterns were formulated concretely as a hypothesis. 
   * *Application, Results*: From the methodically thought-out and detailed documented application of the netlogo model, patterns were recognized and abstracted, and any information about positional relationships was identified. 
   * *Discussion: The* results are discussed under comparison with real room samples (photos). 

- **Good level (9-12 NP)**
   * *Extension level*: functional relationships of the NetLogo template have been optimized. This optimization makes it possible to answer your own question.
   * *Question and hypothesis:* Spatially functional relationships are investigated. For this purpose, a functional relationship between components that are described with sufficient precision for the concrete answer to a question is explicitly formulated as a hypothesis. 
   * *Application, Results:* From the methodically well thought-out and detailed documented application of the netlogo model, a correlation is explained based on the presented model output. 
   * *Discussion: the* significance of the results is discussed statistically or on the basis of other research findings (scientific articles). 

- **Very good level (12-15 NP)**
   * *Extension level:* Compilation of the underlying structures and functions have been optimized. This optimization only makes it possible to answer one's own question. 
   * *Research question and hypothesis:* Spatial processes and interactions are investigated. For this purpose, spatial detail and elements as well as relevant functions are clearly defined and a hypothesis about a process is derived. 
   * *Application and results:* Through the methodically well thought-out and detailed documented execution of the simulation process, data are generated. These are used to verify the hypothesis in a methodically justified way. (e.g. Why do the data speak for the hypothesis?).
   * *Discussion: The* limitations of the results are tested and discussed by varying the simulation. Additional discussion using other research approaches or primary spatial data is positive.


For all levels, the best possible evaluation requires a formally, linguistically and logically largely error-free processing. The following table  serves to support these criteria. The so-called KO criteria automatically lead to a formal fail (f.f.). 

## List of high level requirements

| **Requirements** | |
|The formal conventions of the delivery were not observed (deadline, formats naming etc.)| f.f. |
|Missing list of sources/literature, missing references in the text to literature or illustrations, plagiarism in the text/illustrations.|f.f.|
|The work shows that the subject area defined by the question has not been penetrated at least sufficiently or that the student has fundamental gaps in the prerequisite knowledge.| f.f. |
|The knowledge interest in the topic is recognizable and communicated| mandatory |
|The work is based on a defined problem or question| mandatory |
|Literature/source references are available, the list of sources/literature and the citation method are correct and consistent and assigned to the references in the text| mandatory |
|If used, figure and table captions/numbering are available| mandatory |
|If used, references in the text to figures, maps, diagrams or tables are provided| mandatory |
|Scientifically, technically and factually adequate sources were used.| mandatory |
|The work is logical in structure and argumentation, solutions are comprehensible and justified. The texts are written without mental leaps and/or logical breaks.| mandatory |

*Table: Requirement descriptions Formalia and KO*

##  References

Ammoneit R.,Reudenbach C., Turek A.,Nauß T & Cpeter (2019): Geographische Modellierkompetenz – Modellierung von Raum konzeptualisieren. GW-Unterricht 156 (4/2019), 19–29.

Baade, J., Gertel, H. & A. Schlottmann, (2005): Wissenschaftlich arbeiten. Ein Leitfaden für Studierende der Geographie. (UTB 2630) Verlag Haupt Bern. 

Backhaus, N., & R. Tuor, (2008): Leitfaden für wissenschaftliches Arbeiten. – 7. Auf. – Zürich, (Schriftenreihe / Humangeographie 18) [elektron. Ressource] [https://www.geo.uzh.ch/dam/jcr:a8456d7c-df2d-4603-813a-b1b6c201225b/Leitfaden_v7_0.pdf ](https://www.geo.uzh.ch/dam/jcr:a8456d7c-df2d-4603-813a-b1b6c201225b/Leitfaden_v7_0.pdf)(Stand: 2008, Zugriff: 28.01.2020) 

Bänsch, A. (2003): Wissenschaftliches Arbeiten: Seminar- und Diplomarbeiten. 8. Aufl. München: Oldenbourg. 112 S. ISBN 3-486-27355-8. 

Behrendes L., Cermak, J., Opgenoorth, L., & C. Reudenbach, (2007): Allgemeine Geschäftsbedingungen (AGB v. 0.7), FB 19 Marburg, Diskussionspapier,[elektron. Ressource] <http://www.uni-marburg.de/fb19/personal/wiss_ma/reudenbach/courses>. (Stand: 15.10.2007), (Zugriff: 10.02.2009)

Boles, D., (2002): Leitfaden zur Durchführung individueller Projekte, <http://www.informatik.uni-oldenburg.de/studium/leitfaeden/Leitfaden-IP.pdf>. (Stand:19.09.2002, Zugriff 1.10.2008).

BSc. PO (2016): Studien- und Prüfungsordnung für den Studiengang Geographie/Geography des Fachbereichs Geographie mit dem Abschluss Bachelor of Science (B.Sc.) der Philipps-Universität Marburg vom Stand 2016. [elektron. Ressource] [https://www.uni-marburg.de/de/universitaet/administration/recht/studprueo/01-bachelorstudiengaenge/po-geographie-ba-dritte-aenderung-10052017.pdf ](https://www.uni-marburg.de/de/universitaet/administration/recht/studprueo/01-bachelorstudiengaenge/po-geographie-ba-dritte-aenderung-10052017.pdf)(Stand: 2016, Zugriff: 28.01.2020)

L3 PO (2017): Studien- und Prüfungsordnung für den Studiengang Geographie/Geography des Fachbereichs Geographie mit dem Abschluss Lehramt an Gymnasien der Philipps-Universität Marburg vom Stand 2017. [elektron. Ressource] [https://www.uni-marburg.de/de/universitaet/administration/recht/studprueo/03-lehramt/48_2013.pdf ](https://www.uni-marburg.de/de/universitaet/administration/recht/studprueo/03-lehramt/48_2013.pdf)Stand: 2017, Zugriff: 28.01.2020)

DFG (1998):Vorschläge zur Sicherung guter wissenschaftlicher Praxis: Empfehlungen der Kommission „Selbstkontrolle in der Wissenschaft“; Denkschrift = Proposals for safeguarding good scientific practice / Deutsche Forschungsgemeinschaft. –Weinheim: Wiley-VCH, [elektron. Ressource] [http://www.dfg.de/aktuelles_presse/reden_stellungnahmen/download/empfehlung_wiss_praxis_0198.pdf. ](http://www.dfg.de/aktuelles_presse/reden_stellungnahmen/download/empfehlung_wiss_praxis_0198.pdf.ZU)(Stand: 1998, Zugriff: 24.11.2010)

HSchulG HE (2010): Hessisches Hochschulgesetz vom 01.01.2010. [elektron. Ressource] [http://www.hessen.de/irj/servlet/prt/portal/prtroot/slimp.CMReader/HMWK_15/HMWK_Internet/med/594/5942de1d-cbd9-521f-012f-31e2389e4818,22222222-2222-2222-2222-222222222222 ](http://www.hessen.de/irj/servlet/prt/portal/prtroot/slimp.CMReader/HMWK_15/HMWK_Internet/med/594/5942de1d-cbd9-521f-012f-31e2389e4818,22222222-2222-2222-2222-222222222222)(Stand: 01.01.2010), (Zugriff: 31.01.2010)

Lorenzen, K. (2002): Wissenschaftliche Anforderungen an Diplomarbeiten und Kriterien ihrer Beurteilung [elektron. Ressource] 3. Ausg. HAW Hamburg, FB B/I. [http://www.bui.fh-hamburg.de/fileadmin/redaktion/diplom/lorenzen_wissenschaftliche_anforderungen_dipl.pdf ](http://www.bui.fh-hamburg.de/fileadmin/redaktion/diplom/lorenzen_wissenschaftliche_anforderungen_dipl.pdf)(Stand: 10.2.2002, Zugriff: 24.11.2010)

Philipps-Universität Marburg (2006): Grundsätze und Verfahrensregeln für den Umgang bei wissenschaftlichem Fehlverhalten an der Philipps-Universität Marburg. [elektron. Ressource] <<https://www.uni-marburg.de/de/forschung/profil/ombudsperson/fehlverhalten.pdf>> (Stand: 2011, Zugriff: 28.01.2020).

Philipps-Universität Marburg Fachbereich Geographie (2007): Verbindliche Richtlinien zur Anfertigung von Referaten, schriftlichen Hausarbeiten, Diplom-, Bachelor- und Masterarbeiten im Fachbereich Geographie.[elektron. Ressource] <https://www.uni-marburg.de/de/fb19/studium/pruefungsamt/formulare/dateien/richtlinien2016.pdf>. (Stand: 10/2016, Zugriff: 28.01.2020)


[^1]: The structure and some individual criteria are (in parts) based on: Boles, D., (2002): Guide to the implementation of individual projects. Overall, Behrendes et al. (2007) AGB 0.7 serves as the basis for the evaluation criteria. The AGB is based on the assumption that the students have dealt with scientific working techniques to a sufficient extent. For this, Backhaus & Tuor, (2008), Leitfaden für wissenschaftliches Arbeiten, is assumed. For all other bibliographic references, see bibliography.