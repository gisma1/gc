
This web site is part of gisma spatial science ressources of the [Department of Geography](https://www.uni-marburg.de/fb19).


The content is developed and hosted on Github. 

The responsibility for the content rests with the instructors. Statements, opinions and/or conclusions are the ones from the instructors and do not necessarily reflect the opinion of the representatives of Marburg University.  

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.


The course is based on the Jekyll version of the "Forty" theme by [HTML5 UP](https://html5up.net/).  
Repository [Jekyll logo](https://github.com/jekyll/brand) icon licensed under a [Creative Commons Attribution 4.0 International License](http://choosealicense.com/licenses/cc-by-4.0/).

![gisma GitHub stats](https://github-readme-stats.vercel.app/api?username=gisma&show_icons=true)
